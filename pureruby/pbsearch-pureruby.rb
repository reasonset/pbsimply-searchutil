#!/usr/bin/ruby
require 'oj'

class PBSearch
  class PureRuby
    def initialize(query, **options)
      @query = query
      @options = options || {}
    end

    attr :config, true

    def search
      matched = []
      if @options[:and]
        Dir.children(@config["index_dir"]).each do |i|
          next if i[0] == "." || !File.file?(File.join @config["index_dir"],i)
          match_str = nil
          data = Oj.load(File.read(File.join @config["index_dir"],i))
          @query.each do |q|
            if data["title"].downcase.include?(q)
              match_str ||= {
                pre: "",
                matched_word: "",
                post: data["body"][0, 512]
              }
            elsif index = data["body"].downcase.index(q)
              match_str ||= {
                pre: data["body"][0, index].reverse[0, 512].reverse,
                matched_word: data["body"][index, q.size],
                post: data["body"][(index+q.size), 512]
              }
            else
              match_str = nil
              break
            end
          end
          if match_str
            matched.push({
              "pre" => match_str[:pre],
              "post" => match_str[:post],
              "matched_word" => match_str[:matched_word],
              "title" => data["title"],
              "date" => data["date"],
              "url" => data["frontmatter"]["page_url"]
            })
          end
        end
        matched
      else
        Dir.children(@config["index_dir"]).each do |i|
          next if i[0] == "." || !File.file?(File.join @config["index_dir"],i)
          match_str = nil
          data = Oj.load(File.read(File.join @config["index_dir"],i))
          @query.each do |q|
            if data["title"].downcase.include?(q)
              match_str ||= {
                pre: "",
                matched_word: "",
                post: data["body"][0, 512]
              }
              break
            elsif index = data["body"].downcase.index(q)
              match_str ||= {
                pre: data["body"][0, index].reverse[0, 512].reverse,
                matched_word: data["body"][index, q.size],
                post: data["body"][(index+q.size), 512]
              }
              break
            end
          end
          if match_str
            matched.push({
              "pre" => match_str[:pre],
              "post" => match_str[:post],
              "matched_word" => match_str[:matched_word],
              "title" => data["title"],
              "date" => data["date"],
              "url" => data["frontmatter"]["page_url"]
            })
          end
        end
        matched
      end
    end

    def match
      query = []
      @query.each do |i|
        i = i.gsub(/\s+/, "")
        next if i =~ /^[\s{}:"]+$/
        next if i.length < 2
        query.push i.downcase
      end

      @query = query

      return [] if @query.empty?

      search
    end
  end
end