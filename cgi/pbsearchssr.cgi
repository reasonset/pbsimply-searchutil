#!/usr/bin/ruby
require 'shellwords'
require 'rack'
require 'rackup'
require 'oj'
require_relative 'pbsearch-pureruby'
require_relative 'config'

class PBSearch
  class CGI
    def escape_html str=""
      (str || "")
        .gsub('&', '&amp;')
        .gsub('<', '&lt;')
        .gsub('>', '&gt;')
        .gsub('"', '&quot;')
    end

    def call env
      res = Rack::Response.new
      req = Rack::Request.new env

      params = req.params
      @q = Array === params["q"] ? params["q"][0] : params["q"]
      @smode = Array === params["mode"] ? params["mode"][0] : params["mode"]
      search = PBSearch::PureRuby.new((@q || "").shellsplit, and: (@smode == "and"))
      search.config = CONFIG
      result = search.match
      erb = ERB.new(TEMPLATE, trim_mode: "%<>")
      res.content_type = "text/html; charset=utf-8"
      res.write erb.result(binding)
      res.finish
    end
  end
end

Rackup::Handler::CGI.run PBSearch::CGI.new