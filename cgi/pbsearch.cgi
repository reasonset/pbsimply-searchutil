#!/usr/bin/ruby
require 'shellwords'
require 'rack'
require 'rackup'
require 'oj'
require_relative 'pbsearch-pureruby'
require_relative 'config'

class PBSearch
  class CGI
    def call env
      res = Rack::Response.new
      req = Rack::Request.new env

      params = req.params
      @q = Array === params["q"] ? params["q"][0] : params["q"]
      @smode = Array === params["mode"] ? params["mode"][0] : params["mode"]
      search = PBSearch::PureRuby.new((@q || "").shellsplit, and: (@smode == "and"))
      search.config = CONFIG
      result = search.match
      res.content_type = "application/json; charset=utf-8"
      res.write Oj.dump result
      res.finish
    end
  end
end

Rackup::Handler::CGI.run PBSearch::CGI.new