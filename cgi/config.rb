PBSearch::CONFIG = {
  "index_dir" => "/srv/http/foosite/.search_index"
}

PBSearch::TEMPLATE = <<'EOF'
<html>
  <head>
    <title>Search Result</title>
  </head>
  <body>
    <div id="PB_SearchResult">
      <dl>
% result.sort_by {|i| i["date"] }.reverse_each do |i|
        <dt><a href="<%= ERB::Util.url_encode(i["url"]) %>"><%= escape_html(i["title"]) %></a> <span class="article_date"><%= Time.at(i["date"]).strftime("%F") %></a></dt>
        <dd><%= escape_html(i["pre"].reverse[0, 32].reverse) %><em><%= escape_html(i["matched_word"]) %></em><%= escape_html(i["post"][0, 128]) %></dd>
% end
      </dl>
    </div>
  </body>
</html>
EOF