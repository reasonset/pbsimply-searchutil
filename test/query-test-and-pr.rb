#!/usr/bin/ruby
require_relative '../pureruby/pbsearch-pureruby'
require 'yaml'

config = YAML.load(File.read ".pbsimply.yaml")

query = ARGV.clone

search = PBSearch::PureRuby.new(query, and: true)
search.config = {"index_dir" => config["searchindex_outdir"]}

pp search.match