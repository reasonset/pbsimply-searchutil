#!/usr/bin/ruby
require 'webrick'

server = WEBrick::HTTPServer.new({
  DocumentRoot: Dir.pwd,
  BindAddress: "127.0.1.1",
  CGIInterpreter: WEBrick::HTTPServlet::CGIHandler::Ruby,
  Port: 8080
})

server.mount("/", WEBrick::HTTPServlet::CGIHandler, "pbsearchssr.cgi")
Signal.trap(:INT) {server.shutdown}
server.start