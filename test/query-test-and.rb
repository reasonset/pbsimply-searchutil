#!/usr/bin/ruby
require_relative '../grep-complex/pbsearch-libgrep'
require 'yaml'

config = YAML.load(File.read ".pbsimply.yaml")

query = ARGV.clone

search = PBSearch::GrepComplex.new(query, and: true)
search.config = {"index_dir" => config["searchindex_outdir"]}

pp search.match