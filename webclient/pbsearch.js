const pbsearch_api = "https://example.com/search"
const result_container = document.getElementById("PB_SearchResultContainer")
const pbsearch_showdate = true

async function pbsearch() {
  const this_url = new URL(location.href)
  const result = await fetch(pbsearch_api + this_url.search)

  const result_html = document.createElement("dl")
  result_html.id = "PB_SearchResult"

  const data = await result.json()
  data.sort((a,b) => a.date < b.date ? 1 : a.date > b.date ? -1 : 0)

  for (const i of data) {
    const dt = document.createElement("dt")
    const dt_text = document.createElement("a")
    dt_text.href = i.url
    dt_text.appendChild(document.createTextNode(i.title))
    dt.appendChild(dt_text)
    if (pbsearch_showdate) {
      const article_date = new Date(i.date * 1000) // Convert sec to ms
      const article_date_element = document.createElement("span")
      article_date_element.className = "pbsearch_article_date"
      article_date_element.appendChild(document.createTextNode(article_date.toLocaleDateString()))
      dt.appendChild(article_date_element)
    }
    const dd = document.createElement("dd")

    const pre_body = document.createTextNode(i.pre.slice(-32))
    dd.appendChild(pre_body)

    if (i.matched_word) {
      const matched_body = document.createElement("em")
      matched_body.appendChild(document.createTextNode(i.matched_word))
      dd.appendChild(matched_body)
    }

    const post_body = document.createTextNode(i.post.slice(0, 128))
    dd.appendChild(post_body)
    
    result_html.appendChild(dt)
    result_html.appendChild(dd)
  }

  result_container.appendChild(result_html)
}

pbsearch()