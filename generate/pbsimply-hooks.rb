#!/usr/bin/ruby
require 'digest/rmd160'
require 'oj'

class PBSimply::SerachUtils
  def self.digest style, procdoc
    case style
    when "kramdown"
      require 'kramdown'
      require 'nokogiri'
      Nokogiri::HTML(Kramdown::Document.new(File.read procdoc).to_html).text.gsub(/ +$/, "")
    when "redcarpet"
      require 'redcarpet'
      require 'redcarpet/render_strip'
      md = Redcarpet::Markdown.new(Redcarpet::Render::StripDown)
      md.render(File.read procdoc)
    when "cmark"
      require 'commonmarker'
      doc = CommonMarker.render_doc(File.read procdoc)
      doc.to_plaintext
    else
      IO.popen(["pandoc", "-t", "plain", "--wrap", "none", procdoc], "r") {|io| next io.read}
    end
  end
end

def (PBSimply::Hooks).load_hooks h
  outdir = h.config["searchindex_outdir"] || File.join(h.config["outdir"], ".search_index")

  # Add index
  h.process.add do |arg|
    next if arg[:frontmatter]["source_filename"][0] == "."
    next if h.config["searchindex_include"] && !h.config["searchindex_include"].any? {|i| File.fnmatch(i, arg[:frontmatter]["normalized_docpath"])}
    next if h.config["searchindex_exclude"] && h.config["searchindex_exclude"].any? {|i| File.fnmatch(i, arg[:frontmatter]["normalized_docpath"])}
    key = Digest::RMD160.hexdigest arg[:frontmatter]["page_url"]
    Dir.mkdir outdir unless File.exist? outdir
    date = arg[:frontmatter]["date"].respond_to?(:to_time) ? arg[:frontmatter]["date"].to_time.to_i : arg[:frontmatter]["date"].to_i
    STDERR.puts "Create index for #{arg[:frontmatter]["normalized_docpath"]}"
    File.open(File.join(outdir, key), "w") do |f|
      f.puts(Oj.dump({
        "title" => arg[:frontmatter]["title"],
        "date" => date,
        "body" => PBSimply::SerachUtils.digest(h.config["pbsimply_processor"], arg[:procdoc]).gsub(/\s+/, " "),
        "frontmatter" => arg[:frontmatter]
      }))
    end
  end

  # Remove from index
  h.delete.add do |arg|
    this_url = (arg[:source_file_path]).sub(/^[\.\/]*/) { h.config["self_url_prefix"] || "/" }.sub(/\.[a-zA-Z0-9]+$/, ".html") # Same as PureBuilder Simply
    key = Digest::RMD160.hexdigest this_url
    if File.exist? File.join(outdir, key)
      STDOUT.puts "Delete deleted article search index #{this_url} (#{key})"
      File.delete File.join(outdir, key)
    end
  end
end