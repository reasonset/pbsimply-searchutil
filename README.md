# Synopsis

Web content search framework work with PureBuilder Simply 3.

# Description

This software is intended to extend the functionality of PureBuilder Simply and add article search capabilities.

Basically, it is envisioned that users will use this software as a resource to extend and modify it to develop search capabilities that fit your website.
Since PureBuilder Simply assumes that the server functionality can serve files statically, the environment in which the web application runs will vary greatly from user to user, and it is not practical to provide the search software in a form that will work in all user's environments.

However, if your server environment happens to match the assumptions of this software, or if you build your server to fit the assumptions of this software, you may be able to use this software as is.

This software works with PureBuilder Simply 3.2.1 or later because it uses the Hooks feature added in PureBuilder Simply 3.0 and the configuration file access feature added in 3.2.1.

The early version of pbsimply-searchutils is fundamentally different from the current version with a specification that builds an index from built HTML files and searches on that index.
If what you are looking for is closer to the early version, you can access the early version in the branch named `1.x`.

# Requirement

* Ruby >=3.0
* Oj Gem

## Indexer

* PureBuilser Simply >=3.2.1
* (Use kramdown)
    * Kramdown gem
    * Nokogiri gem
* (Use RedCarpet)
    * RedCarpet gem
* (Use CommonMarker)
    * CommonMarker gem

## CGI app

* Rack gem
* Rackup gem

## Rackup app

* Rack gem
* (Optional) this or puma gem

# Usage

## Generate indexes

1. copy `generate/pbsimply-hooks.rb` to `.pbsimply-hooks.rb` on your document root.
    * If your hooks file is already exist, please merge files.
2. set config `searchindex_outdir`
    * If not set, use `${outdir}/.serach_index` by default.
3. (Optional) set config `searchindex_include` (Array)
    * If it is set, index only `normalized_docpath` matched with any of it as Glob pattern.
4. (Optional) set config `searchindex_exclude` (Array)
    * If it is set, don't index `normalized_docpath` matched with any of it as Glob pattern.
5. Rebuild all documents

## Server Side (CGI)

Copy `cgi/config.rb` and edit it to fit your site.
`PBSearch::TEMPLATE` is not used.

and upload `pbsearch-pureruby.rb`, `pbsearch.cgi` and `config.rb` to your CGI path.

## Server Side (CGI/Server Side Rendering)

Copy `cgi/config.rb` and edit it to fit your site.
If you want, edit `PBSearch::TEMPLATE` too.

and upload `pbsearch-pureruby.rb`, `pbsearchssr.cgi` and `config.rb` to your CGI path.

## Server Side (Rackup)

Create config file `config.rb` defines `PBSearch::CONFIG` like this

```ruby
PBSearch::CONFIG = {
  "index_dir" => "/srv/http/foosite/.search_index"
}
```

and upload `pbsearch-pureruby.rb`, `pbsearch.ru` and `config.rb` to your application directory, and start with `rackup pbsearch.ru`.

You can be run in the manner of a typical web application.

## Client side (with Pandoc)

### Put javascript file

Edit `pbsearch_api` param in `pbsearch.js` to fit your server and upload it.

### Pandoc template

Create "search page" like this

```markdown
---
title: Search Result
searchpage: yes
---

:::{#PB_SearchResultContainer}
:::
```

and edit template to load search script on search page.

```
<head>
<!-- ... -->
$if(searchpage)$
<script src="pbsearch.js"></script>
$endif$
```