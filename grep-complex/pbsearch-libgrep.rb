#!/usr/bin/ruby
require 'oj'

class PBSearch
  class GrepComplex
    def initialize(query, **options)
      @query = query
      @options = options || {}
    end

    attr :config, true

    def list_candidate
      cmd = ["grep", "-rFil"]
      @query.each do |i|
        cmd.push "-e"
        cmd.push i
      end
      list = []
      IO.popen([*cmd, config["index_dir"]], "r") do |io|
        io.each do |line|
          list.push line.chomp
        end
      end

      list
    end

    def search candidates
      matched = []
      if @options[:and]
        candidates.each do |i|
          match_str = nil
          data = Oj.load(File.read(i))
          @query.each do |q|
            if data["title"].downcase.include?(q)
              match_str ||= {pre: "", post: data["body"][0, 512]}
            elsif index = data["body"].downcase.index(q)
              match_str ||= {pre: data["body"][(index < 512 ? 0 : index - 512), 512], post: data["body"][index, 512]}
            else
              match_str = nil
              break
            end
          end
          if match_str
            matched.push({
              pre: match_str[:pre],
              post: match_str[:post],
              title: data["title"],
              date: Time.at(data["date"]),
              url: data["page_url"]
            })
          end
        end
        matched
      else
        candidates.each do |i|
          match_str = nil
          data = Oj.load(File.read(i))
          @query.each do |q|
            if data["title"].downcase.include?(q)
              match_str ||= {pre: "", post: data["body"][0, 512]}
              break
            elsif index = data["body"].downcase.index(q)
              match_str ||= {pre: data["body"][(index < 512 ? 0 : index - 512), 512], post: data["body"][index, 512]}
              break
            end
          end
          if match_str
            matched.push({
              pre: match_str[:pre],
              post: match_str[:post],
              title: data["title"],
              date: Time.at(data["date"]),
              url: data["page_url"]
            })
          end
        end
        matched
      end
    end

    def match
      query = []
      @query.each do |i|
        i = i.gsub(/\s+/, "").downcase
        next if i =~ /^[\s{}:"]+$/
        next if "title".include? i
        next if "date".include? i
        next if "frontmatter".include? i
        next if i.length < 2
        query.push i
      end

      @query = query

      return [] if @query.empty?

      candidates = list_candidate
      search candidates
    end
  end
end